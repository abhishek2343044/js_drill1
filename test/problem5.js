let years = require('./problem4.js');

var car_years = years();
var old_cars = [];

function OldCars() {
  for (index = 0; index < car_years.length; index++) {
    if (car_years[index] < 2000) {
      old_cars.push(car_years[index]);
    }
  }
  return old_cars;
}
module.exports = OldCars;
