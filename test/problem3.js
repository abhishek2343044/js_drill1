let inventory = require('./problem.js');
var car_model_list = [];

function car_models() {
  for (index = 0; index < inventory.length; index++) {
    car_model_list.push(inventory[index].car_model);
  }
  return car_model_list.sort();
}
module.exports = car_models;
