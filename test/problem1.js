let inventory = require('./problem.js');

function recall(car_id) {
  console.log(
    `Car ${car_id} is a ${inventory[car_id - 1].car_year} ${
      inventory[car_id - 1].car_make
    } ${inventory[car_id - 1].car_model}`
  );
}
module.exports = recall;
